from django.shortcuts import render
from lab_1.views import mhs_name, birth_date

#TODO Implement
#Create a content paragraph for your landing page:
landing_page_content = 'Saya Jihan Amalia Irfani, disini saya belajar PPW dan ini adalah percobaan saya dalam Lab PPW. Ini merupakan Landing Page untuk Lab Kedua. Klik link ini untuk informasi lebih lanjut. Happy read!'

def index(request):
    response = {'name': mhs_name, 'content': landing_page_content}
    return render(request, 'index_lab2.html', response)