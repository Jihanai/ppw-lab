1
    1) Implementasi fungsi Login & Logout
	index dipanggil, mengecek apakah user sudah login, kalau belum akan dialihkan ke halaman login
    Di dalam halaman login ketika disubmit akan memanggil fungsi auth_login di custom_auth yang akan
    mengecek validitas username dan password dengan memanggil csui_helper. Apabila data sudah valid
    maka akan dipanggil fungsi index dengan mengubah flag login menjadi true. Setelah itu akan masuk
    ke profile. Untuk logout, tinggal masuk ke auth_logout dan menghapus session yang ada.

    2) Jelaskan Bagaimana Django Apps kalian mengetahui bahwa setiap user yang menagkses Apps kalian,
akan dilayani secara berbeda? (Data milik pengguna tidak akan saling tertukar). Tunjukkan didalam baris kode
	Semua data2 mengenai user seperti nama dan kode identitas disimpan di dalam models. Selain itu,
    semua daftar watch later juga disimpan di dalam database yang memliki foreign key yang direference
    ke objek Pengguna, sehingga data yang disimpan sudah relevan dan tidak tertukar

class Pengguna(models.Model):
    kode_identitas = models.CharField('Kode Identitas', max_length=20, primary_key=True, )
    nama = models.CharField('Nama', max_length=200)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class MovieKu(models.Model):
    pengguna = models.ForeignKey(Pengguna)
    kode_movie = models.CharField("Kode Movie", max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

2
    1)Mendaftar dan menggunakan OMDb API untuk menampilkan daftar film
	Set API key untuk bisa mengakses OMDB dengan mendaftarkannya di website. Setelah itu fungsi yang
    digunakan adalah fungsi search_movie untuk mengakses API dan mendapatkan film berdasarkan search
    input yang diketik
    2)Menggunakan DataTables untuk menampilkan daftar film dan filtering
	Di dalam ombdb API diambil hanya 30 film saja yang paling banyak. Untuk bagian html, di file
    list.html terdapat method untuk DataTable yang berisi AJAX untuk mengambil data
    3)Membuat fungsi pencarian film berdasarkan judul dan tahun
	Fungsi di dalam omdb_api yang digunakan adalah search_movie yang menerima parameter judul film
    dan tahun. Dari fungsi tersebut akan memanggil API omdb dan mendapatkan kembalian berupa json berisi
    data-data film yang memiliki judul hampir sama seperti yang disearch tadi.
    4) Sudah dijelaskan di atas

3
    1)Membuat daftar film "tonton nanti" (disimpan di session)
	Diimplementasikan ketika kondisi user belum login dan menambahkan film ke watch later. Ada di views
    di bagian add_watch_later. Setelah itu, dicek apakah film tersebut sudah ada di dalam session. Kalau
    sudah ada berarti terdeteksi bahwa user menginput secara manual ke url.
    2)Membuat daftar film "watch later" (disimpan di model)
	Di dalam views, apabila user sudah login maka data watch later akan dimasukkan ke dalam database.
    3)Jelaskan Bagaimana Django Apps kalian bisa menyimpan film yang ingin ditonton, tanpa Login terlebih dahulu?
	Menyimpan data film di dalam session, apabila id film tersebut sudah ada, maka tidak akan disimpan
    lagi. Terdapat di bagian utils.py. Dalam setiap request pasti terdapat session yang menyimpan data-data
    sementara client. Session tersebut yang digunakan untuk menyimpan data watch later ketika user belum
    login.
    4)  Jelaskan Bagaimana Django Apps kalian bisa menyimpan film yang ingin ditonton kedalam database, dan membedakan
kepemilikan dari massing - masing daftar film? Tunjukkan dalam baris kode

def create_new_user(request):
    nama = get_data_user(request, 'user_login')
    kode_identitas = get_data_user(request, 'kode_identitas')

    pengguna = Pengguna()
    pengguna.kode_identitas = kode_identitas
    pengguna.nama = nama
    pengguna.save()

    return pengguna


def dashboard(request):
    #print ("#==> dashboard")

    if not 'user_login' in request.session.keys():
        return HttpResponseRedirect(reverse('lab-10:index'))
    else:
        set_data_for_session(request)
        kode_identitas = get_data_user(request, 'kode_identitas')
        try:
            pengguna = Pengguna.objects.get(kode_identitas = kode_identitas)
        except Exception as e:
            pengguna = create_new_user(request)

        movies_id = get_my_movies_from_session(request)
        save_movies_to_database(pengguna, movies_id)

        html = 'lab_10/dashboard.html'
        return render(request, html, response)
def add_watch_later(request, id):
    #print ("ADD WL => ", id)
    msg = "Berhasil tambah movie ke Watch Later"
    if get_data_user(request, 'user_login'):
        #print ("TO DB")
        is_in_db = check_movie_in_database(request, id)
        if not is_in_db:
            add_item_to_database(request, id)
        else:
            msg = "Movie already exist on DATABASE! Hacking detected!"
    else:
        #print ("TO SESSION")
        is_in_ssn = check_movie_in_session(request, id)
        if not is_in_ssn:
            add_item_to_session(request, id)
        else:
            msg = "Movie already exist on SESSION! Hacking detected!"

    messages.success(request, msg)
    return HttpResponseRedirect(reverse('lab-10:movie_detail', args=(id,)))

def add_item_to_database(request, id):
    kode_identitas = get_data_user(request, 'kode_identitas')
    pengguna = Pengguna.objects.get(kode_identitas=kode_identitas)
    movieku = MovieKu()
    movieku.kode_movie = id
    movieku.pengguna = pengguna
    movieku.save()

def get_data_user(request, tipe):
    data = None
    if tipe == "user_login" and 'user_login' in request.session:
        data = request.session['user_login']
    elif tipe == "kode_identitas" and 'kode_identitas' in request.session:
        data = request.session['kode_identitas']

    return data

	Create user dulu kalau belum pernah login. Setelah itu, ketika user klik add watch later, maka akan
    otomatis menyimpan data film ke database. Fungsinya terdapat di utils.py. Fungsi yang ada di utils ini
    akan dipanggil apabila user sudah login, yang dicek di views.

4
    1) Sudah dari lab 1
    2) Mengecek bentuk status code yang dikembalikan apabila memasukkan url tertentu. Selain itu, mengecek
    jenis-jenis message yang ditampilkan, jika memang di fungsi tersebut diimplementasikan. Yang perlu dicek
    juga response data film yang dikembalikan. Berbagai kondisi seperti di omdb_api juga perlu dites.
