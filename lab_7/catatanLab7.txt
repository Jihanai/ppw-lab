Mandatory
1. Membuat halaman untuk menampilkan semua mahasiswa fasilkom
  -Terdapat list yang berisi daftar mahasiswa fasilkom, yang dipanggil dari django model.
	Di dalam fungsi index, dibuat key dari dictionarynya adalah mahasiswa_list
 dan valuenya berisi query models yang mengambil semua data di dalam objek Friend.
	Setelah itu, di dalam file lab_7.html dicek apabila ada temannya, maka dilakukan
 looping untuk meletakkan data teman tersebut di halaman.

	dalam fungsi index tersebut terdapat paginator untuk membagi data dalam satu halaman ke 10 data dan menampilkan paginator
  -Buatlah tombol untuk dapat menambahkan list mahasiswa kedalam daftar teman (implementasikan menggunakan ajax).
	Di setiap baris dalam mahasiswa_list terdapat button yang apabila ditekan akan
 memanggil fungsi js addFriend
	Dari fungsi js tersebut, akan membuat method POST
 dan membuat url add-friend
	Apabila belum ada NPM yang terdaftar di database, maka
 akan ditambahkan di dalam daftar teman, jika sudah akan muncul pesan error.

  -Mengimplentasikan validate_npm untuk mengecek apakah teman yang ingin dimasukkan sudah ada didalam daftar teman atau belum.
	melengkapi fungsi validate_npm pada views.py dan mengecek apakah friend dengan npm tersebut sudah ada atau belum dengan filter(..)
  -Membuat pagination (hint: salah satu data yang didapat dari kembalian api.cs.ui.ac.id adalah next dan previous yang bisa digunakan dalam membuat pagination)
	pada lab_7.html ditambahkan html untuk paginate pagenya
	Di fungsi index akan memanggil fungsi paginate dengan parameter mahasiswa_list.
	Setelah itu, dibuat objek Paginator dan menecek apakah pagenya bukan integer, atau
 page kosong
	Setelah itu, dicari max index yang didapat dari banyaknya index, dalam
 suatu halaman.
	Kemudian, mengembalikan dictionary data_paginate yang berisi data
 mahasiswa dan page_rangenya
 lalu akan dirender oleh fungsi index
2. Membuat halaman untuk menampilkan daftar teman
  -Terdapat list yang berisi daftar teman, data daftar teman didapat menggunakan ajax.
	membuat html untuk tampilan list daftar teman (daftar_teman.html) dengan penggunaan ajax dan header.html untuk redirect pada list teman
	pertama-tama akan meminta list teman yang ada pada views.py (get_friend_list) yan dilakukan serialize untuk mendapatkan format json
	lalu data tersebut akan dikembalikan kembali, jika tidak ada teman maka akan menampilkan alert
	jika ada, maka akan ditambahkan html untuk nama-nama dalam list dan tombol delete.
  -Buatlah tombol untuk dapat menghapus teman dari daftar teman (implementasikan menggunakan ajax).
	jika tombol delete ditekan, akan masuk kedalam fungsi ajax deleteFriend untuk menghapus teman.
	fungsi tersebut akan menjalankan fungsi delete_friend pada views.py berdasarkan id / pk mahasiswa
	lalu redirect kembali pada friend-list
3. Pastikan kalian memiliki Code Coverage yang baik
	ditambahkan test case yang berkaitan dengan fungsi

Additional 
2. Berkas ".env" untuk menyimpan username dan password, dapat menyebabkan akun anda terbuka untuk orang yang memiliki 
akses ke repository bila berkas tersebut ter-push ke repository.
Hal ini sangat tidak baik dan bisa memalukan karena dapat membuka rahasia/privacy anda sendiri.
	membuat .env untuk mengisi data ssonya. Lalu pada .gitignore ditambahkan .env agar tidak ter push ke repo.